QUnit.test("Here's a test that should always pass", function (assert) {
    assert.ok(1 == "1", "1=1 success!");
});

QUnit.test('Testing calculate function with several sets of inputs', function (assert) {
    //assert.equal(calculate(, "sine"), NaN, "Tested for sine");
    assert.equal(calculate(0, "sine"), 0, "Tested for sine");
    assert.equal(calculate(-1, "sine"), -0.8414709848078965, "Tested for sine");
    assert.equal(calculate(1, "sine"), 0.8414709848078965, "Tested for sine");
    assert.equal(calculate(0, "cos"), 1, "Tested for cos");
    assert.equal(calculate(-1, "cos"), 0.5403023058681398, "Tested for cos");
    assert.equal(calculate(1, "cos"), 0.5403023058681398, "Tested for cos");
    assert.equal(calculate(0,"tan"),0,"tested for tan");
    assert.equal(calculate(1,"tan"),1.5574077246549023,"tested for tan");
    assert.equal(calculate(-1,"tan"), -1.5574077246549023,"tested for tan");
    //assert.equal(calculate(, "sine"), 0.8414709848 ,'Tested with value=1');
        //throws( block                                    [, expected ] [, message ] ) 
   // assert.throws(function () { calculate(null); }, /The given argument is not a number/, 'Passing in null correctly raises an Error');
        //throws( block                                    [, expected ] [, message ] ) 
    //assert.throws(function () { calculate("Christine","Christine"); }, /The given argument is not a number/, 'Passing in a string correctly raises an Error');
});

